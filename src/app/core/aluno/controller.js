define(function(require) {
  'use strict';

  var module = require('./module');

  module.controller('AlunoCtrl', AlunoCtrl);

  //---

  AlunoCtrl.$inject = ['$scope', 'AlunoResource', '$state'];

  function AlunoCtrl($scope, AlunoResource, $state) {
    
    $scope.model = {

    };

    $scope.salvar = salvarFn;
    $scope.buscar = findAllFn;
    $scope.showInfos = showInfosFn;
    $scope.showHabilidades = showHabilidadesFn;
    $scope.showCaracteristicas = showCaracteristicasFn;
    $scope.onSelectedAluno = onSelectedAlunoFn;

    $scope.habilidades = false;
    $scope.infos = true;
    $scope.caracteristicas = false;

    function findAllFn(){
      AlunoResource.query(function(response) {
        $scope.alunos = response;
      });
    }

    function salvarFn(){
      console.log($scope.model);
      AlunoResource.save($scope.model, function(response) {
        $scope.myWelcome = response.data;
      });
    }

    function showInfosFn() {
      $scope.habilidades = false;
      $scope.infos = true;
      $scope.caracteristicas = false;
    }

    function showHabilidadesFn() {
      $scope.habilidades = true;
      $scope.infos = false;
      $scope.caracteristicas = false;
    }

    function showCaracteristicasFn() {
      $scope.habilidades = false;
      $scope.infos = false;
      $scope.caracteristicas = true;
    }

    function onSelectedAlunoFn( aluno ) {
      $state.go( 'home' );
    }

  }

});
