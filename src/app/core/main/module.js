define(function(require) {
  'use strict';

  var angular = require('angular');

  require('uiRouter');
  require('angularResource');

  return angular.module(

    'main',

    [
      'ui.router',
      'ngResource'
    ]
  );

});
