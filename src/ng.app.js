define(function(require) {
  'use strict';

  var angular = require('angular');

  angular.element(document).ready(startAngularApp);

  //---

  function startAngularApp() {

    var module = angular.module(
      'run',

      [
        require('app/core/package').name
      ]
    );

    angular.bootstrap( document, [module.name] );
  }

});
